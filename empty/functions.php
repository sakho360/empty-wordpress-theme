<?php

    define('THEME_VERSION', '1.0.0');
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	remove_filter( 'the_content', 'wpautop' );

	// Included Scripts
    if (file_exists(get_template_directory() . '/included/script.php')){
        require_once(get_template_directory() . '/included/script.php');
    }

    // Included Styles
    if (file_exists(get_template_directory() . '/included/styles.php')){
        require_once(get_template_directory() . '/included/styles.php');
    }

    // Create a function for register_nav_menus()
	function add_wp3menu_support() {
	 
		register_nav_menus(
			array(
			'primary' => __( 'Primary Menu' ),
			'footer-menu' => __( 'Footer Menu' )
			)
		 );

	}
	
	//Add the above function to init hook.

    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => __('Sidebar Widgets','html5reset' ),
    		'id'   => 'sidebar-widgets',
    		'description'   => __( 'These are widgets for the sidebar.','html5reset' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }

add_action('add_attachment', 'attachment_manipulation');
function attachment_manipulation($id)
{
    if(wp_attachment_is_image($id)){
        //do your own tasks
    }

}
?>