<?php

function include_scripts() {

    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('app_vendor', (get_stylesheet_directory_uri() . '/js/app_vendor.min.js'), '', THEME_VERSION, true);
        wp_enqueue_script('app_vendor');

        if (file_exists(get_template_directory() . '/js/app.min.js')) {
            wp_register_script('app', (get_stylesheet_directory_uri() . '/js/app.min.js'), '', THEME_VERSION, true);
            wp_enqueue_script('app');
        } else {
            $scripts = json_decode(file_get_contents(get_stylesheet_directory_uri() . '/included/custom_scripts.json'));
            foreach ($scripts as $index => $script){
                wp_register_script($index, (get_stylesheet_directory_uri() .'/'. $script), '', THEME_VERSION, true);
                wp_enqueue_script($index);
            }
        }

    }
}

add_action( 'wp_enqueue_scripts', 'include_scripts' );



